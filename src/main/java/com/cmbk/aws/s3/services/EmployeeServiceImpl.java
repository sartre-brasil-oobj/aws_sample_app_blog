package com.cmbk.aws.s3.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmbk.aws.s3.domain.CreateEmployeeRequest;
import com.cmbk.aws.s3.domain.CreateEmployeeResponse;
import com.cmbk.aws.s3.domain.Employee;
import com.cmbk.aws.s3.domain.EmployeeResponse;
import com.cmbk.aws.s3.domain.Status;
import com.cmbk.aws.s3.domain.common.exception.BadRequestException;
import com.cmbk.aws.s3.domain.common.exception.NotFoundException;
import com.cmbk.aws.s3.domain.transform.EmployeeTransformer;
import com.cmbk.aws.s3.infrastructure.AWSConfig;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author chanaka.k
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

	static final Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	private static final String EMP = "emp_";

	private ObjectMapper mapper = new ObjectMapper()
			.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);

	@Autowired
	private AWSConfig awsConfig;

	@Override
	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest) {

		logger.info("Inside create employee service");

		Employee employee = null;
		CreateEmployeeResponse response = null;

		try {
			employee = EmployeeTransformer.transformEmployeeRequestToDto(createEmployeeRequest);
			// Convert java object into String JSON type.
			ObjectMapper objectMapper = new ObjectMapper();
			// Upload file into S3 bucket
			awsConfig.uploadFile(EMP + employee.getId(), String.valueOf(objectMapper.writeValueAsString(employee)));

			response = new CreateEmployeeResponse();
			Status status = new Status();
			status.setStatusCode("SUCCESS");
			status.setStatusDescription(EMP + employee.getId() + " file successfully uploaded");
			response.setStatus(status);

		} catch (Exception e) {
			logger.error("Error occured while uploading the file");
			response = new CreateEmployeeResponse();
			Status status = new Status();
			status.setStatusCode("FAILED");
			status.setStatusDescription("File upload process been failed");
			response.setStatus(status);
			return response;
		}

		logger.info("Successfully saved into the S3 bucket");

		return response;
	}

	@Override
	public EmployeeResponse retrieveEmployeeById(String employeeId) {

		logger.info("Inside retrieve employee by id, service");

		Employee employee = null;
		EmployeeResponse response = null;

		try {

			employee = this.getEmployeeDetailsByUserId(employeeId);
			response = EmployeeTransformer.employeeToEmployeeResponse(employee);

		} catch (Exception e) {
			logger.error("Error occured while retrieving the file");
			throw new NotFoundException("Error occured while retrieving the file");
		}

		logger.info("Successfully returned employee by id");
		return response;
	}

	private Employee getEmployeeDetailsByUserId(String employeeId)
			throws JsonMappingException, JsonProcessingException {

		String strVersion = awsConfig.retrieveFileContentAsString(employeeId);

		return mapper.readValue(strVersion, Employee.class);
	}

	@Override
	public void removeEmployeeById(String employeeId) {

		try {
			awsConfig.removeFileById(employeeId);
		} catch (Exception e) {
			logger.error("Error occured while removing the file");
			throw new BadRequestException("Error occured while removing the file");
		}

	}

}
