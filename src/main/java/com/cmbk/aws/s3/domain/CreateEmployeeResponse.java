package com.cmbk.aws.s3.domain;

/**
 * @author chanaka.k
 *
 */
public class CreateEmployeeResponse {

	private Status status;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
