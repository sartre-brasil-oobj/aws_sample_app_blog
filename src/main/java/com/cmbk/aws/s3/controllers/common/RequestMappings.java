package com.cmbk.aws.s3.controllers.common;

/**
 * @author chanaka.k
 *
 */
public interface RequestMappings {
	
	public static String EMPLOYEES = "employees";
		
	public static String CREATE_EMPLOYEE = "/add";
	
	public static String RETRIVE_EMPLOYEE_BY_ID = "/{employee-id}";
	
	public static String REMOVE_EMPLOYEE = "/{employee-id}";
	
	
}
